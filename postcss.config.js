module.exports = {
  plugins: {
    // postcss  工具箱
    // postcss 其中的一个工具
    // postcss-pxtorem 插件的版本需要 >= 5.0.0
    'postcss-pxtorem': {
      // 形参是一个对象 可以直接解构
      rootValue ({ file }) {
        // 判断处理的文件路径是否包含 vant
        // 如果包含vant 处理的vant 375  37.5
        // 如果不包含vant 处理的我们自己的样式 750 75
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      }, // 将多少px转化为rem
      propList: ['*']
    }
  }
}
