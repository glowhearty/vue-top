// 封装搜索的请求
import request from '@/utils/request.js'

/**
 * 根据关键字获取搜索建议
 * @param {*} q 搜索关键字
 * @returns peomise
 */
export const getSearchSuggestion = q => {
  return request({
    url: '/v1_0/suggestion',
    method: 'GET',
    params: {
      q
    }
  })
}

/**
 * 根据关键字获取搜索结果
 * @param {*} params { page, per_page, q }
 * @returns promise
 */
export const getSearchResults = params => {
  return request({
    url: '/v1_0/search',
    method: 'GET',
    params
  })
}
