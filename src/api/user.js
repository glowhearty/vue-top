import request from '@/utils/request.js'
/**
 * 发送验证码
 * @param {*} mobile 手机号
 */
export const sendCode = mobile => {
  return request({
    url: '/v1_0/sms/codes/' + mobile
  })
}

/**
 * 用户登录
 * @param {*} data { mobile, code }
 * @returns promise
 */
export const login = data => {
  return request({
    url: '/v1_0/authorizations',
    method: 'POST',
    data
  })
}

/**
 * 获取用户自己信息
 * @returns promise
 */
export const getUserInfo = () => {
  return request({
    url: '/v1_0/user',
    method: 'GET'
  })
}

/**
 * 获取用户频道列表
 * @returns promise
 */
export const getUserChannels = () => {
  return request({
    url: '/v1_0/user/channels',
    method: 'GET'
  })
}

/**
 * 添加用户关注
 * @param {*} target 关注的用户id
 * @returns promise
 */
export const addFollowUser = target => {
  return request({
    url: '/v1_0/user/followings',
    method: 'POST',
    data: {
      target
    }
  })
}

/**
 * 删除关注用户
 * @param {*} target 用户id
 * @returns promise
 */
export const removeFollowUser = target => {
  return request({
    url: '/v1_0/user/followings/' + target,
    method: 'DELETE'
  })
}

/**
 * 获取用户信息
 * @returns promise
 */
export const getUserProfile = () => {
  return request({
    url: '/v1_0/user/profile',
    method: 'GET'
  })
}

/**
 * 更新用户信息
 * @param {*} data { name?, photo?, birthday?, gender? }
 * @returns promise
 */
export const updateUserProfile = data => {
  return request({
    url: '/v1_0/user/profile',
    method: 'PATCH',
    data: data
  })
}

/**
 * 更新用户头像
 * @param {*} data formData对象
 * @returns promise
 */
export const updateUserAvatar = data => {
  return request({
    url: '/v1_0/user/photo',
    method: 'PATCH',
    data
  })
}
