import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage.js'

Vue.use(Vuex)

const hmttToken = 'HMTT-TOKEN'

// this.$store = 下面暴漏的这个实例
export default new Vuex.Store({
  state: {
    user: getItem(hmttToken)
  },
  mutations: {
    // 给user赋值
    setUser (state, payload) {
      // 将数据存到state
      state.user = payload
      // 将数据存本地
      setItem(hmttToken, payload)
    }
  },
  actions: {
  },
  modules: {
  }
})
