import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Layout from '@/views/Layout'
import Home from '@/views/Home'
import Qa from '@/views/Qa'
import Video from '@/views/Video'
import Profile from '@/views/Profile'
import Search from '@/views/Search'
import Article from '@/views/Article'
import User from '@/views/User'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: Login
  },
  // {
  //   path: '/',
  //   redirect: '/home'
  // },
  {
    path: '/',
    component: Layout,
    children: [
      {
        // 默认子路由
        path: '',
        component: Home
      },
      {
        path: '/video',
        component: Video
      },
      {
        path: '/qa',
        component: Qa
      },
      {
        path: '/profile',
        component: Profile
      }
    ]
  },
  {
    path: '/article/:articleId',
    component: Article,
    props: true // 开启props传参
  },
  {
    path: '/search',
    component: Search
  },
  {
    path: '/user',
    component: User
  }
]

const router = new VueRouter({
  routes
})

export default router
