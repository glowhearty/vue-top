import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './vant'
// 设置html根节点字体大小
import 'amfe-flexible'
import './styles/index.less'
import '@/filters'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
