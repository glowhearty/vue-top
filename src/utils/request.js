import axios from 'axios'
import { Toast } from 'vant'
import store from '@/store'
import JSONBig from 'json-bigint'
// axios.defaults.baseURL = 'http://ttapi.research.itcast.cn/'
// axios.defaults.baseURL = '另外一套'
const request = axios.create({
  // 后台返回原始数据
  transformResponse: [function (data) {
    // 对 data 进行任意转换处理
    // JSONBig.parse 不能处理普通字符串 只能处理JSON字符串
    try {
      return JSONBig.parse(data)
    } catch (err) {
      return data
    }
  }],
  baseURL: 'http://toutiao-app.itheima.net'
  // baseURL: 'http://ttapi.research.itcast.cn/'
})

// 使用实例发送请求和axios发送请求 一摸一样

// 给request 添加请求拦截器
// 发送请求前可以进行统一的配置
// 拦截所有的请求
// 保存没有toast提示的
const notLoading = ['/v1_1/articles', '/v1_0/search', '/v1_0/comments']
request.interceptors.request.use(config => {
  if (store.state.user) {
    // config: 请求配置信息
    config.headers.Authorization = 'Bearer ' + store.state.user.token
  }
  // /v1_0/articles/ 详情的url
  // config.url.indexOf('/v1_0/articles/') !== -1 请求的接口地址 详情的
  // config.url 请求的url
  // /v1_0/articles/1981
  // /v1_0/article/collections
  const isDetail = /\/v1_0\/articles\/\d+/.test(config.url)
  if (notLoading.indexOf(config.url) === -1 && !isDetail) {
    Toast.loading({
      message: '加载中...',
      duration: 0, // 持续展示
      forbidClick: true // 会显示一个透明的遮罩
    })
  }
  return config
})

// 响应拦截器
request.interceptors.response.use(res => {
  Toast.clear()
  return res.data.data
})

export default request
