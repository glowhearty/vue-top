import Vue from 'vue'

import * as filters from './filters.js'

Object.keys(filters).forEach(item => {
  Vue.filter(item, filters[item])
})

// console.log(filters)

// Vue.filter('formatDate', formatDate)
// Vue.filter('strFormat', strFormat)
