import dayjs from 'dayjs'
// 导入语言包
import 'dayjs/locale/zh-cn'
// 导入相对时间插件
import relativeTime from 'dayjs/plugin/relativeTime'
// 给dayjs集成相对时间的方法
dayjs.extend(relativeTime)
// 使用语言包
dayjs.locale('zh-cn')
// console.log(dayjs().to('2000'))
// 全局过滤器
export const formatDate = (data) => {
  return dayjs().to(data)
}

// 处理字符串
export const strFormat = str => {
  return str + '哈哈'
}
